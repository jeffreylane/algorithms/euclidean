package euclidean

// tail recursive. calls itself until greatest common denominator of a and b is found and returned.
func GCD(a, b int) int {
	if a == 0 {
		return b
	}
	if b == 0 {
		return a
	}

	var large, small int
	if a > b {
		large, small = a, b
	} else {
		large, small = b, a
	}

	remainder := large % small
	if remainder == 0 {
		return small
	}

	return GCD(small, remainder)
}
