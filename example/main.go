package main

import (
	"fmt"

	"gitlab.com/jeffreylane/algorithms/euclidean"
)

func main() {
	a, b := 48, 32
	gcd := euclidean.GCD(a, b)
	fmt.Println(gcd)
}
